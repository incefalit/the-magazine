﻿using System.ComponentModel.DataAnnotations;

namespace Magazine.RequestModels
{
    public class CommentGetRequestModel
    {
        [Range(1, int.MaxValue, ErrorMessage = "Id of the recipe should be positive integer")]
        [Required]

        public int DishId { get; set; }
    }
}
