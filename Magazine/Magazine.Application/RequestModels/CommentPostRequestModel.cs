﻿using System.ComponentModel.DataAnnotations;

namespace Magazine.RequestModels
{
    public class CommentPostRequestModel
    {
        [Required]
        [StringLength(255, MinimumLength = 2, ErrorMessage = "Comment text must not exceed 255 characters and be less than 2")]
        public string CommentText { get; set; } = string.Empty;

        [Range(1, int.MaxValue, ErrorMessage = "Id of the recipe should be positive integer")]
        [Required]
        public int DishId { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "Id of the user should be positive integer")]
        [Required]
        public int UserId { get; set; }
    }
}
