using AutoMapper;

using Magazine.Application.Dto.User;
using Magazine.Data.Persistent.Entity;
using Magazine.Application.Dto.Step;
using Magazine.Application.Dto.Comments;
using Magazine.Application.Dto.Dish;
using Magazine.Application.Dto.User;
using Magazine.RequestModels;

namespace Magazine.Application.Mapper;

public class AppMappingProfile : Profile
{
    public AppMappingProfile()
    {
	    CreateMap<Step, StepDto>();
        CreateMap<User, GetUserDto>();

		CreateMap<Step, StepDto>();
        CreateMap<User, GetUserDto>();
        CreateMap<User, UserGetDtoWithFollowers>()
            .ForMember(x => x.userFollowers, y => y.MapFrom(z => z.Followers.Count))
            .ForMember(x => x.recipeCount, y => y.MapFrom(z => z.DishAuthors.FirstOrDefault().Dishes.Count));
        CreateMap<Step, StepDto>();
        CreateMap<Comment, CommentGetDto>()
            .ForMember(c => c.UserName, c => c.MapFrom(c => c.User.Name))
            .ForMember(c => c.UserImage, c => c.MapFrom(c => c.User.Image));
        CreateMap<Dish, GetDishByIdDto>()
            .ForMember(x => x.DishAuthor, s => s.MapFrom(x => x.DishAuthor.User.Name));
        CreateMap<Ingredient, GetIngredientsDto>()
            .ForMember(x => x.ProductName, s => s.MapFrom(x => x.Products.FirstOrDefault().Name))
            .ForMember(x => x.ProductImage, s => s.MapFrom(x => x.Products.FirstOrDefault().Image))
            .ForMember(x => x.ProductAmount, s => s.MapFrom(x => x.ProductAmount))
            .ForMember(x => x.ProductUnitName, s => s.MapFrom(x => x.ProductUnit.Unit));
        CreateMap<User, UserTagDto>();
        CreateMap<Dish, GetDishForUserDto>();
        CreateMap<CommentPostRequestModel, CommentPostDto>();
        CreateMap<CommentPostDto, Comment>();
    }
}