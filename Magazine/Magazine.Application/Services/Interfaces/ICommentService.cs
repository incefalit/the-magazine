﻿using Magazine.Application.Dto.Comments;

namespace Magazine.Application.Services.Interfaces
{
    public interface ICommentService
    {
        Task<List<CommentGetDto>> GetCommentsAsync(int dishId);

        Task InsertAsync(CommentPostDto comment);
    }
}
