﻿using Magazine.Application.Dto.User;

namespace Magazine.Application.Services.Interfaces
{
    public interface IUserService
    {
        public Task<GetUserDto> GetByIdAsync(int userId);

        public Task<UserGetDtoWithFollowers> GetByUserByIdWithFollowersAsync(int userId);
    }
}
