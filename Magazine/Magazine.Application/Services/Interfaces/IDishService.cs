﻿using Magazine.Application.Dto.Dish;
using Magazine.Application.Dto.Step;
using Magazine.Application.Dto.User;

namespace Magazine.Application.Services.Interfaces;

public interface IDishService
{
    public Task<List<StepDto>> GetStepsByDishIdAsync(GetStepsByDishIdDto dishId);

    public Task<GetDishByIdDto> GetIngredientsByIdAsync(int dishId);

    public Task<List<UserTagDto>> GetTagsUsersAsync(GetUsersTagsDto name);

    public Task<List<GetDishForUserDto>> GetAllDishesByUserIdAsync(int userId, bool isPublished);

    public Task<List<GetDishForUserDto>> GetAllSavedDishesByUserIdAsync(int userId);
}