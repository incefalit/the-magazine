﻿using AutoMapper;

using Magazine.Application.Dto.Comments;
using Magazine.Application.Services.Interfaces;
using Magazine.Data.Persistent.Entity;
using Magazine.Data.Persistent.Repository.Interfaces;

using Serilog;

namespace Magazine.Application.Services.MagazineService
{
    public class CommentService : ICommentService
    {
        private readonly ICommentRepository _commentRepository;

        private readonly IMapper _mapper;

        public CommentService(ICommentRepository commentRepository, IMapper mapper)
        {
            _commentRepository = commentRepository;
            _mapper = mapper;
        }

        public async Task<List<CommentGetDto>> GetCommentsAsync(int dishId)
        {
            var comments = await _commentRepository.GetCommentsByDishIdAsync(dishId);
            var commentDtos = comments.Select(comment => _mapper.Map<CommentGetDto>(comment)).ToList();

            Log.Information($"Get a list with {commentDtos.Count} comments.");

            return commentDtos;
        }

        public async Task InsertAsync(CommentPostDto commentPostDto)
        {
            var comment = _mapper.Map<Comment>(commentPostDto);
            comment.CommentDate = DateTime.UtcNow;
            await _commentRepository.InsertAsync(comment);
        }
    }
}
