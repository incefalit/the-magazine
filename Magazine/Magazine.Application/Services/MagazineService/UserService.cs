﻿using AutoMapper;

using Magazine.Application.Dto.User;
using Magazine.Application.Services.Interfaces;
using Magazine.Data.Persistent.Repository.Interfaces;

namespace Magazine.Application.Services.MagazineService
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;

        private readonly IMapper _mapper;

        public UserService(IUserRepository userRepository, IMapper mapper)
        {
            _userRepository = userRepository;
            _mapper = mapper;
        }


        public async Task<GetUserDto> GetByIdAsync(int userId)
        {
            var user = await _userRepository.GetByIdAsync(userId);
            var userDto = _mapper.Map<GetUserDto>(user);
            return userDto;
        }

        public async Task<UserGetDtoWithFollowers> GetByUserByIdWithFollowersAsync(int userId)
        {
            var user = await _userRepository.GetByIdWithFollowersAsync(userId);
            var userDto = _mapper.Map<UserGetDtoWithFollowers>(user);
            return userDto;
        }
    }
}

