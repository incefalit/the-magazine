﻿using AutoMapper;

using Magazine.Application.Dto.Dish;
using Magazine.Application.Dto.Step;
using Magazine.Application.Dto.User;
using Magazine.Application.Services.Interfaces;
using Magazine.Data.Persistent.Repository.Interfaces;

using Serilog;

namespace Magazine.Application.Services.MagazineService
{
    public class DishService : IDishService
    {
        private readonly IStepRepository _stepRepository;
        private readonly IDishRepository _dishRepository;
        private readonly IUserRepository _userRepository;
        private readonly IMapper _mapper;

        public DishService(
            IStepRepository stepRepository, 
            IUserRepository userRepository, 
            IMapper mapper, 
            IDishRepository dishRepository)
        {
            _stepRepository = stepRepository;
            _userRepository = userRepository;
            _dishRepository = dishRepository;
            _mapper = mapper;
        }

        public async Task<List<StepDto>> GetStepsByDishIdAsync(GetStepsByDishIdDto dto)
        {
            var steps = await _stepRepository.GetStepsByDishIdAsync(dto.DishId);
            var stepDtos = steps
                .Select(step => _mapper.Map<StepDto>(step))
                .ToList();

            Log.Information($"Get a list with {stepDtos.Count} steps.");

            return stepDtos;
        }

        public async Task<GetDishByIdDto> GetIngredientsByIdAsync(int dishId)
        {
            var dish = await _dishRepository.GetIngredientAsync(dishId);
            var dishDto = _mapper.Map<GetDishByIdDto>(dish);

            Log.Information($"Get a dish list with ingredients {dish}.");

            return dishDto;
        }

        public async Task<List<UserTagDto>> GetTagsUsersAsync(GetUsersTagsDto dto)
        {
            var users = await _userRepository.GetAllUsersTagsAsync(dto.Name);
            var usersTagsDtos = users
                .Select(user => _mapper.Map<UserTagDto>(user))
                .ToList();

            Log.Information($"Get a list with {usersTagsDtos.Count} tags.");

            return usersTagsDtos;
        }

        public async Task<List<GetDishForUserDto>> GetAllDishesByUserIdAsync(int userId, bool isPublished)
        {
            var dishes = await _dishRepository.GetAllDishesByUserIdAsync(userId, isPublished);
            var dishesDto = dishes
                .Select(dish => _mapper.Map<GetDishForUserDto>(dish))
                .ToList();

            Log.Information($"Get a list with {dishesDto.Count} dishes");

            return dishesDto;
        }

        public async Task<List<GetDishForUserDto>> GetAllSavedDishesByUserIdAsync(int userId)
        {
            var dishes = await _dishRepository.GetAllSavedDishesByUserIdAsync(userId);
            var dishesDto = dishes
                .Select(dish => _mapper.Map<GetDishForUserDto>(dish))
                .ToList();

            Log.Information($"Get a list with {dishesDto.Count} saved dishes");

            return dishesDto;
        }
    }
}
