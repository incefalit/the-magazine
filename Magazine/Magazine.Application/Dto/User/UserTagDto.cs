﻿namespace Magazine.Application.Dto.User;

public class UserTagDto
{
    public int Id { get; set; }

    public string Name { get; set; }

    public string Image { get; set; }
}