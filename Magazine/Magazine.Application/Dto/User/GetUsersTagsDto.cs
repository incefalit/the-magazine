﻿namespace Magazine.Application.Dto.User;

public class GetUsersTagsDto
{
    public string Name { get; set; }
}