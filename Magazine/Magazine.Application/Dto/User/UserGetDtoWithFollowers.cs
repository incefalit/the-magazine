﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Magazine.Application.Dto.User
{
    public class UserGetDtoWithFollowers
    {
        public int userFollowers { get; set; }

        public int recipeCount { get; set; }

        public string Name { get; set; }

        public string Image { get; set; }
    }
}
