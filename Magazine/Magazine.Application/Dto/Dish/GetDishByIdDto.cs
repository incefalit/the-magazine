﻿namespace Magazine.Application.Dto.Dish;

public class GetDishByIdDto
{
    public int Id { get; set; }

    public string Name { get; set; }

    public string DishAuthor { get; set; }

    public string Image { get; set; }

    public int DishAuthorId { get; set; }

    public int Time { get; set; }

    public int Calories { get; set; }

    public int Protein { get; set; }

    public int Fat { get; set; }

    public int Carb { get; set; }

    public List<GetIngredientsDto> Ingredients { get; set; }
}