﻿namespace Magazine.Application.Dto.Dish;

public class GetIngredientsDto
{
    public string ProductName { get; set; }

    public string ProductImage { get; set; }

    public int ProductAmount { get; set; }

    public string ProductUnitName { get; set; }
}