﻿namespace Magazine.Application.Dto.Step;

public class StepDto
{
    public int Id { get; set; }

    public int Count { get; set; }

    public string Info { get; set; }

    public string Image { get; set; }
}