﻿namespace Magazine.Application.Dto.Step;

public class GetStepsByDishIdDto
{
    public int DishId { get; set; }
}