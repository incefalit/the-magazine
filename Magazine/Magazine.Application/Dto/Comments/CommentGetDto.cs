﻿
namespace Magazine.Application.Dto.Comments
{
    public class CommentGetDto
    {
        public string CommentText { get; set; }

        public string UserName { get;set; }

        public string UserImage { get; set; }

        public DateTime CommentDate { get; set; }
    }
}
