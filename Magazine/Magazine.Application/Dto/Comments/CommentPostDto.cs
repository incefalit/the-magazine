﻿
namespace Magazine.Application.Dto.Comments
{
    public class CommentPostDto
    {
        public string CommentText { get; set; }

        public int UserId { get; set; }
        
        public int DishId { get; set; }
    }
}
