
﻿using Magazine.Data.Persistent;
﻿using Magazine.Data.Persistent.Entity;

﻿using Magazine.Data.Persistent;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace MagazineTest.TestWebApplicationFactories;

public class TestWebApplicationFactory<TStartup>
    : WebApplicationFactory<TStartup> where TStartup : class
{
    protected override void ConfigureWebHost(IWebHostBuilder builder)
    {
        builder.ConfigureServices(services =>
        {
            var descriptor = services.SingleOrDefault(
                d => d.ServiceType == typeof(DbContextOptions<ApplicationDbContext>));

            if (descriptor != null)
            {
                services.Remove(descriptor);
            }

            services.AddDbContext<ApplicationDbContext>(options =>
            {
                options.UseInMemoryDatabase("InMemoryDbForTesting");
            });

            var serviceProvider = services.BuildServiceProvider();
            InitializeDatabase(serviceProvider);
        });
    }

    private static void InitializeDatabase(IServiceProvider serviceProvider)
    {
        using var scope = serviceProvider.CreateScope();
        var scopedServices = scope.ServiceProvider;
        var applicationDbContext = scopedServices.GetRequiredService<ApplicationDbContext>();

        applicationDbContext.Database.EnsureCreated();
        applicationDbContext.Steps.AddRange(GetTestSteps());
        applicationDbContext.Comments.AddRange(GetTestCommentsByDishId());
        applicationDbContext.Users.AddRange(GetTestsUsers());
        applicationDbContext.Dishes.AddRange(GetTestsDishes());
        applicationDbContext.SaveChanges();
    }


    private static IEnumerable<User> GetTestsUsers()
    {
        var users = new List<User>
        {
            new() 
            { 
                Id = 1,
                Image = "image_1",
                Name = "name_1",
                SavedDishes = new List<Dish>
                {
                    new()
                    {
                        Id = 1,
                        Publish = true,
                        Name = "name_1",
                        Image = "image_1"
                    },
                    new()
                    {
                        Id = 2,
                        Publish = true,
                        Name = "name_2",
                        Image = "image_2"
                    },
                    new()
                    {
                        Id = 3,
                        Publish = true,
                        Name = "name_3",
                        Image = "image_3"
                    }
                }
            },
            new()
            {
                Id = 2,
                Image = "image_2",
                Name = "name_2"
            },
            new() 
            { 
                Id = 3,
                Image = "image_3",
                Name = "name_3"
            },
            new()
            {
                Id = 4,
                Image = "image_4",
                Name = "name_3"
            },
            new()
            {
                Id = 5,
                Image = "image_5",
                Name = "name_5"
            }
        };

        return users;
    }

    private static IEnumerable<Step> GetTestSteps()
    {
        var steps = new List<Step>
        {
            new()
            {
                DishId = 1,
                Count = 1,
                Id = 1,
                Image = "image_1",
                Info = "info_1"
            },
            new()
            {
                DishId = 1,
                Count = 2,
                Id = 2,
                Image = "image_2",
                Info = "info_2"
            },
            new()
            {
                DishId = 1,
                Count = 3,
                Id = 3,
                Image = "image_3",
                Info = "info_3"
            }
        };

        return steps;
    }

    private static IEnumerable<Comment> GetTestCommentsByDishId()
    {
        var dish = new Dish() { Id = 7, Calories = 2, Carb = 2, DishAuthorId = 1, Image = "url", Name = "namr" };
        var user = new User() { Id = 6, Name = "o", Image = "ghg" };
        var comments = new List<Comment>
        {
            new()
            {
                CommentDate = DateTime.Now,
                CommentText = "comm1",
                Dish = dish,
                User = user,
                DishId = 1,
                Id = 1,
                UserId = 6,
            },
            new()
            {
                CommentDate = DateTime.Now,
                CommentText = "comm2",
                Dish = dish,
                User = user,
                DishId = 1,
                Id = 2,
                UserId = 6,
            },
            new()
            {
                CommentDate = DateTime.Now,
                CommentText = "comm3",
                Dish = dish,
                User = user,
                DishId = 1,
                Id = 3,
                UserId = 6,
            }
        };

        return comments;
    }

    private static IEnumerable<Dish> GetTestsDishes()
    {
        var dishAuthor = new DishAuthor
        {
            Id = 1,
            UserId = 2
        };

        var steps = new List<Dish>
        {
            new()
            {
                Id = 4,
                Name = "egg",
                Image = "egg.png",
                DishAuthorId = 1,
                DishAuthor = dishAuthor,
                Publish = true
            },

            new()
            {
                Id = 5,
                Name = "milkshake",
                Image = "milkshake.png",
                DishAuthorId = 1,
                DishAuthor = dishAuthor,
                Publish = true
            }
        };

        return steps;
    }
}   