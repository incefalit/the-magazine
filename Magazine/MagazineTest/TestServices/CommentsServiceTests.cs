﻿using AutoMapper;

using Magazine.Application.Dto.Comments;
using Magazine.Application.Services.MagazineService;
using Magazine.Data.Persistent.Entity;
using Magazine.Data.Persistent.Repository.Interfaces;

using Moq;


namespace MagazineTest.TestServices
{
    public class CommentsServiceTests
    {
        [Fact]
        public async Task GetCommentsAsyncTest()
        {
            //arrange
            var user = new User() { Image = "url", Name = "anton" };
            var dish = new Dish();

            var comment1 = new Comment() { UserId = 4, DishId = 2, CommentDate = default, CommentText = "abc", Id = 2, Dish = dish, User = user };
            var comment2 = new Comment() { UserId = 4, DishId = 2, CommentDate = default, CommentText = "lot", Id = 2, Dish = dish, User = user };
            var comment3 = new Comment() { UserId = 4, DishId = 2, CommentDate = default, CommentText = "ukj", Id = 2, Dish = dish, User = user };

            var commentsList = new List<Comment>
            {
                comment1,
                comment2,
                comment3,

            };

            var commentGetDto1 = new CommentGetDto() { UserName = "anton", UserImage = "url", CommentText = "abc", CommentDate = default };
            var commentGetDto2 = new CommentGetDto() { UserName = "anton", UserImage = "url", CommentText = "abc", CommentDate = default };
            var commentGetDto3 = new CommentGetDto() { UserName = "anton", UserImage = "url", CommentText = "abc", CommentDate = default };

            var resultList = new List<CommentGetDto>
            {
                commentGetDto1,
                commentGetDto2,
                commentGetDto3,
            };

            var mockRepository = new Mock<ICommentRepository>();
            mockRepository.Setup(repo => repo.GetCommentsByDishIdAsync(2))
                .ReturnsAsync(commentsList);

            var commentDto = new CommentGetDto();

            var mockAutoMapper = new Mock<IMapper>();
            mockAutoMapper
                .Setup(x => x.Map<CommentGetDto>(It.IsAny<Comment>()))
                .Returns(commentDto);

            var service = new CommentService(mockRepository.Object, mockAutoMapper.Object);

            //act
            var actionResult = await service.GetCommentsAsync(2);

            //assert
            Assert.Equal(resultList.Count(), actionResult.Count);
        }
    }
}
