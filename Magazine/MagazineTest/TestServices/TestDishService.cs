﻿using AutoMapper;

using Magazine.Application.Dto.Dish;
using Magazine.Application.Dto.Step;
using Magazine.Application.Dto.User;
using Magazine.Application.Services.MagazineService;
using Magazine.Data.Persistent.Entity;
using Magazine.Data.Persistent.Repository.Interfaces;

using Moq;

using FluentAssertions;

namespace MagazineTest.TestServices
{
    public class TestDishService
    {
        [Fact]
        public async Task GetStepsByDishIdAsync_Return_Steps()
        {
            // Arrange
            var mockRepository = new Mock<IStepRepository>();
            mockRepository
                .Setup(repo => repo.GetStepsByDishIdAsync(1))
                .ReturnsAsync(GetTestSteps());

            var stepDto = new StepDto();
            var mockAutoMapper = new Mock<IMapper>();
            mockAutoMapper
                .Setup(x => x.Map<StepDto>(It.IsAny<Step>()))
                .Returns(stepDto);

            var dishService = new DishService(mockRepository.Object, null!, mockAutoMapper.Object, null);
            var getStepsByDishIdDto = new GetStepsByDishIdDto
            {
                DishId = 1
            };

            // Act
            var actionResult = dishService.GetStepsByDishIdAsync(getStepsByDishIdDto);

            // Assert
            var contentResult = await actionResult;
            var result = GetTestStepsDto().Except(contentResult);

            contentResult.Should().NotBeNullOrEmpty();
            contentResult.Count.Should().Be(result.Count());
            mockRepository.Verify(mock => mock.GetStepsByDishIdAsync(1), Times.Once());
            mockAutoMapper.Verify(mock => mock.Map<StepDto>(It.IsAny<Step>()), Times.AtLeastOnce());
        }

        [Fact]
        public async Task GetTagsUsersAsync_Return_TagsUser()
        {
            // Arrange
            var mockRepository = new Mock<IUserRepository>();
            mockRepository
                .Setup(repo => repo.GetAllUsersTagsAsync("name"))
                .ReturnsAsync(GetTestUsers);

            var userTagDto = new UserTagDto();
            var mockAutoMapper = new Mock<IMapper>();
            mockAutoMapper
                .Setup(x => x.Map<UserTagDto>(It.IsAny<User>()))
                .Returns(userTagDto);

            var dishService = new DishService(null!, mockRepository.Object, mockAutoMapper.Object, null);
            var getUsersTagsDto = new GetUsersTagsDto()
            {
                Name = "name"
            };

            // Act
            var actionResult = dishService.GetTagsUsersAsync(getUsersTagsDto);

            // Assert
            var contentResult = await actionResult;
            var result = GetTestUserTagsDto().Except(contentResult);

            contentResult.Should().NotBeNullOrEmpty();
            contentResult.Count.Should().Be(result.Count());
            mockRepository.Verify(mock => mock.GetAllUsersTagsAsync("name"), Times.Once());
            mockAutoMapper.Verify(mock => mock.Map<UserTagDto>(It.IsAny<User>()), Times.AtLeastOnce());
        }

        [Fact]
        public async Task GetDishIdServiceTest()
        {
            var mock = new Mock<IDishRepository>();
            mock.Setup(x => x.GetIngredientAsync(2))
                .ReturnsAsync(GetIngredientIdList());

            var dishDto = new GetDishByIdDto();
            var mockAutoMapper = new Mock<IMapper>();
            mockAutoMapper
                .Setup(x => x.Map<GetDishByIdDto>(It.IsAny<Dish>()))
                .Returns(dishDto);

            var service = new DishService(null, null, mockAutoMapper.Object, mock.Object);
            var result = await service.GetIngredientsByIdAsync(2);

            Assert.NotNull(result);
            Assert.IsAssignableFrom<GetDishByIdDto>(result);
            mock.Verify(mock => mock.GetIngredientAsync(2), Times.Once());
            mockAutoMapper.Verify(mock => mock.Map<GetDishByIdDto>(It.IsAny<Dish>()), Times.AtLeastOnce());
        }

        [Fact]
        public async Task GetAllSavedDished_Return_Correct_Dishes_Numbers()
        {
            var mock = new Mock<IDishRepository>();
            mock.Setup(x => x.GetAllSavedDishesByUserIdAsync(It.IsAny<int>()))
                .ReturnsAsync(GetTestSavedDishes());

            var dishDto = new GetDishForUserDto();
            var mockAutoMapper = new Mock<IMapper>();
            mockAutoMapper
                .Setup(x => x.Map<GetDishForUserDto>(It.IsAny<Dish>()))
                .Returns(dishDto);

            var service = new DishService(null, null, mockAutoMapper.Object, mock.Object);
            var result = await service.GetAllSavedDishesByUserIdAsync(1);

            result.Should().NotBeNull();
            GetTestSavedDishes().Count.Should().Be(result.Count);
            mock.Verify(mock => mock.GetAllSavedDishesByUserIdAsync(1), Times.Once());
            mockAutoMapper.Verify(mock => mock.Map<GetDishForUserDto>(It.IsAny<Dish>()), Times.AtLeastOnce());
        }

        [Fact]
        public async Task GetDishesByUserIdAsync_Return_Dishes()
        {
            // Arrange
            var mockRepository = new Mock<IDishRepository>();
            mockRepository
                .Setup(repo => repo.GetAllDishesByUserIdAsync(2, true))
                .ReturnsAsync(GetDishesByUserDto());

            var dishDto = new GetDishForUserDto();
            var mockAutoMapper = new Mock<IMapper>();
            mockAutoMapper
                .Setup(x => x.Map<GetDishForUserDto>(It.IsAny<Dish>()))
                .Returns(dishDto);

            var dishService = new DishService(null, null, mockAutoMapper.Object, mockRepository.Object);

            // Act
            var actionResult = dishService.GetAllDishesByUserIdAsync(2, true);

            // Assert
            var contentResult = await actionResult;
            var result = GetResultDishesByUserDto().Except(contentResult);

            contentResult.Should().NotBeNullOrEmpty();
            contentResult.Count.Should().Be(result.Count());
            mockRepository.Verify(mock => mock.GetAllDishesByUserIdAsync(2, true), Times.Once());
            mockAutoMapper.Verify(mock => mock.Map<GetDishForUserDto>(It.IsAny<Dish>()), Times.AtLeastOnce());
        }

        private Dish GetIngredientIdList()
        {
            var dish = new Dish()
            {
                Id = 2,
                DishAuthorId = 2,
                Image = "egg.png",
                Name = "egg",
                Time = 20,
                Calories = 310,
                Fat = 320,
                Protein = 30,
                Carb = 30,
                DishAuthor = new DishAuthor()
                {
                    Id = 2,
                    UserId = 2,
                    User = new User()
                    {
                        Id = 2,
                        Image = "igor.png",
                        Name = "Igor"
                    }
                },

                Ingredients = new List<Ingredient>()
                {
                    new()
                    {
                        Id = 2,
                        DishId = 2,
                        ProductId = "2",
                        ProductAmount = 500,
                        Products = new List<Product>()
                        {
                            new()
                            {
                                Id = 2,
                                Image = "imageProduct",
                                Name = "NameProduct",
                                IngredientId = 2
                            }
                        },

                        ProductUnit = new ProductUnit()
                        {
                            Id = 2,
                            Unit = "mg"
                        }
                    }
                },
            };

            return dish;
        }

        private List<Dish> GetTestSavedDishes()
        {
            var userList = new List<Dish> {
                new() { Id = 1, Image = "image_1", Name = "Pizza", Publish = true, DishAuthorId = 1},
                new() { Id = 2, Image = "image_2", Name = "Potato", Publish = true, DishAuthorId = 1 },
                new() { Id = 3, Image = "image_3", Name = "Soup", Publish = true, DishAuthorId = 1},
            };

            return userList;
        }

        private List<Step> GetTestSteps()
        {
            var stepList = new List<Step>()
            {
                new () {Count = 1, Image = "image_1", Id = 1, Info = "info_1", DishId = 1},
                new () {Count = 2, Image = "image_2", Id = 1, Info = "info_2", DishId = 2},
                new () {Count = 3, Image = "image_3", Id = 1, Info = "info_3", DishId = 3},
                new () {Count = 4, Image = "image_4", Id = 1, Info = "info_4", DishId = 4},
                new () {Count = 5, Image = "image_5", Id = 1, Info = "info_5", DishId = 5}
            };

            return stepList;
        }

        private IEnumerable<StepDto> GetTestStepsDto()
        {
            var stepList = new List<StepDto>()
            {
                new () {Count = 1, Image = "image_1", Id = 1, Info = "info_1"},
                new () {Count = 2, Image = "image_2", Id = 1, Info = "info_2"},
                new () {Count = 3, Image = "image_3", Id = 1, Info = "info_3"},
                new () {Count = 4, Image = "image_4", Id = 1, Info = "info_4"},
                new () {Count = 5, Image = "image_5", Id = 1, Info = "info_5"}
            };

            return stepList;
        }

        private List<User> GetTestUsers()
        {
            var userList = new List<User>
            {
                new() { Id = 1, Image = "image_1", Name = "name_1" },
                new() { Id = 2, Image = "image_2", Name = "name_2" },
                new() { Id = 3, Image = "image_3", Name = "name_3" },
                new() { Id = 4, Image = "image_4", Name = "name_3" },
                new() { Id = 5, Image = "image_5", Name = "name_5" }
            };

            return userList;
        }

        private IEnumerable<UserTagDto> GetTestUserTagsDto()
        {
            var userList = new List<UserTagDto>
            {
                new() { Id = 1, Image = "image_1",Name = "name_1" },
                new() { Id = 2, Image = "image_2",Name = "name_2" },
                new() { Id = 3, Image = "image_3",Name = "name_3" },
                new() { Id = 4, Image = "image_4",Name = "name_3" },
                new() { Id = 5, Image = "image_5",Name = "name_5" }
            };

            return userList;
        }

        private List<Dish> GetDishesByUserDto()
        {
            var dishList = new List<Dish>()
            {
                new() {Id = 1, Name = "eggs", Image = "eggs.png"},
                new() {Id = 2, Name = "milkshake", Image = "milkshake.png"},
                new() {Id = 3, Name = "burger", Image = "burger.png"}
            };

            return dishList;
        }

        private IEnumerable<GetDishForUserDto> GetResultDishesByUserDto()
        {
            var dishList = new List<GetDishForUserDto>()
            {
                new() {Id = 1, Name = "eggs", Image = "eggs.png"},
                new() {Id = 2, Name = "milkshake", Image = "milkshake.png"},
                new() {Id = 3, Name = "burger", Image = "burger.png"}
            };

            return dishList;
        }
    }
}