﻿using AutoMapper;

using Magazine.Application.Dto.Step;
using Magazine.Application.Dto.User;
using Magazine.Application.Services.MagazineService;
using Magazine.Data.Persistent.Entity;
using Magazine.Data.Persistent.Repository.Interfaces;

using Moq;

using Newtonsoft.Json;

namespace MagazineTest.TestServices
{
    public class UserServiceTests
    {
        [Fact]
        public async Task GetUserByIdAsync_Return_User()
        {
            // Arrange
            var user = new User
            {
                Id = 1,
                Name = "anton",
                Image = "url",
                Role = new Role(),
                Comments = new List<Comment>(),
                DishAuthors = new List<DishAuthor>(),
                Followers = new List<Follower>(),
                RoleId = 1,
                Friends = new List<Friend>()
            };

            var userDto = new GetUserDto
            {
                Id = 1,
                Name = "anton",
                Image = "url",
            };

            var mockRepository = new Mock<IUserRepository>();
            mockRepository.Setup(repo => repo.GetByIdAsync(1)).ReturnsAsync(user);

            var mockMapper = new Mock<IMapper>();
            mockMapper
               .Setup(x => x.Map<GetUserDto>(It.IsAny<User>()))
               .Returns(userDto);
            var userService = new UserService(mockRepository.Object, mockMapper.Object);

            //Act
            var actionResult = await userService.GetByIdAsync(1);

            //Assert
            var object1Json = JsonConvert.SerializeObject(userDto);
            var object2Json = JsonConvert.SerializeObject(actionResult);
            Assert.Equal(object1Json, object2Json);
        }

        [Fact]
        public async Task GetUserByIdWithFollowersAsync_Return_User()
        {
            // Arrange
            var user = new User
            {
                Id = 1,
                Name = "anton",
                Image = "url",
                Role = new Role(),
                Comments = new List<Comment>(),
                DishAuthors = new List<DishAuthor>(),
                Followers = new List<Follower>(),
                RoleId = 1,
                Friends = new List<Friend>()
            };

            var userGetDtoWithFollowers = new UserGetDtoWithFollowers
            {
                Image = "url",
                Name = "andrey",
                recipeCount = 1,
                userFollowers = 2
            };

            var mockRepository = new Mock<IUserRepository>();
            mockRepository.Setup(repo => repo.GetByIdAsync(1)).ReturnsAsync(user);

            var mockMappper = new Mock<IMapper>();
            mockMappper
                .Setup(x => x.Map<UserGetDtoWithFollowers>(It.IsAny<User>()))
                .Returns(userGetDtoWithFollowers);

            var userService = new UserService(mockRepository.Object, mockMappper.Object);

            // Act
            var actionResult = await userService.GetByUserByIdWithFollowersAsync(1);

            // Assert
            var object1Json = JsonConvert.SerializeObject(userGetDtoWithFollowers);
            var object2Json = JsonConvert.SerializeObject(actionResult);
            Assert.Equal(object1Json, object2Json);
        }
    }
}
