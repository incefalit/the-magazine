﻿using Magazine.Application.Dto.User;
using Magazine.Application.Services.Interfaces;
using Magazine.Controllers;
using Magazine.RequestModels;

using Microsoft.AspNetCore.Mvc;

using Moq;

using Newtonsoft.Json;

namespace MagazineTest.TestControllers
{
    public class UserControllerTests
    {
        [Fact]
        public async Task UserController_GetUser_ReturnUser()
        {
            //arrange
            var userExpected = new GetUserDto() { Id = 1, Name = "andrey", Image = "url" };

            var mockServ = new Mock<IUserService>();
            mockServ.Setup(x => x.GetByIdAsync(1)).ReturnsAsync(userExpected);
            var controller = new UserController(mockServ.Object);

            //act
            var actionResult = (OkObjectResult)await controller.GetUser(new UserGetRequestModel() { Id = 1 });

            //assert
            var object1Json = JsonConvert.SerializeObject(userExpected, Formatting.None, new JsonSerializerSettings()
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            });

            var object2Json = JsonConvert.SerializeObject(actionResult.Value, Formatting.None, new JsonSerializerSettings()
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            });

            Assert.Equal(object1Json, object2Json);
        }

        [Fact]
        public async Task UserController_GetUserProfile_ReturnUser()
        {
            //arrange
            var userExpected = new UserGetDtoWithFollowers() { recipeCount = 1, userFollowers = 2, Name = "andrey", Image = "url" };
            var mockServ = new Mock<IUserService>();
            mockServ.Setup(x => x.GetByUserByIdWithFollowersAsync(1)).ReturnsAsync(userExpected);
            var controller = new UserController(mockServ.Object);
            var userGetRequestModel = new UserGetRequestModel() { Id = 1 };
            //act
            var actionResult = (OkObjectResult)await controller.GetUserByUserIdWithFollowers(userGetRequestModel);

            //assert
            var object1Json = JsonConvert.SerializeObject(userExpected, Formatting.None, new JsonSerializerSettings()
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            });

            var object2Json = JsonConvert.SerializeObject(actionResult.Value, Formatting.None, new JsonSerializerSettings()
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            });

            Assert.Equal(object1Json, object2Json);
        }
    }
}
