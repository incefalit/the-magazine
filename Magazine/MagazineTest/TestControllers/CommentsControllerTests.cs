﻿using AutoMapper;
using FluentAssertions;
using Magazine.Application.Dto.Comments;
using Magazine.Application.Services.Interfaces;
using Magazine.Controllers;
using Magazine.Data.Persistent.Entity;
using Magazine.RequestModels;

using Microsoft.AspNetCore.Mvc;

using Moq;

namespace MagazineTest.TestControllers
{
    public class CommentsControllerTests
    {
        [Fact]
        public async Task CommentsController_GetAll_ReturnOK()
        {
            //arrange
            var commentsGetRequestModel = new CommentGetRequestModel() { DishId = 3 };

            var commentGetDto1 = new CommentGetDto() { UserImage = "ggd", UserName = "abc", CommentText = "Test string 1", CommentDate = default };
            var commentGetDto2 = new CommentGetDto() { UserImage = "ggd", UserName = "abc", CommentText = "Test string 2", CommentDate = default };
            var commentGetDto3 = new CommentGetDto() { UserImage = "ggd", UserName = "abc", CommentText = "Test string 3", CommentDate = default };

            var commentsExpected = new List<CommentGetDto>
            {
                commentGetDto1,
                commentGetDto2,
                commentGetDto3,
            };

            var mockServ = new Mock<ICommentService>();
            mockServ
                .Setup(x => x.GetCommentsAsync(3))
                .ReturnsAsync(commentsExpected);

            var commentDto = new CommentGetDto();
            var mockMapper = new Mock<IMapper>();
            var mockAutoMapper = new Mock<IMapper>();
            mockAutoMapper
                .Setup(x => x.Map<CommentGetDto>(It.IsAny<Comment>()))
                .Returns(commentDto);
            var controller = new CommentsController(mockServ.Object, mockAutoMapper.Object);

            //act
            var result = (OkObjectResult)await controller.GetCommentsByDishId(commentsGetRequestModel);

            //assert
            Assert.NotNull(result);
            Assert.Equal(commentsExpected, result.Value);
            Assert.Equal(200, result.StatusCode);
        }

        [Fact]
        public async Task CommentsController_AddComment_ReturnValue()
        {
            //arrange
            var comment = new CommentPostDto
            {
                CommentText = "hello",
                DishId = 3,
                UserId = 3,
            };

            var commentGetDto1 = new CommentGetDto() { UserImage = "ggd", UserName = "abc", CommentText = "Test string 1", CommentDate = default };
            var commentGetDto2 = new CommentGetDto() { UserImage = "gd", UserName = "abc", CommentText = "Test string 2", CommentDate = default };

            var commentInitial = new List<CommentGetDto>()
            {
                commentGetDto1,
                commentGetDto2,
            };

            var commentGetDto3 = new CommentGetDto() { UserImage = "gg", UserName = "abc", CommentText = "Test string 3", CommentDate = default };

            var commentsResult = new List<CommentGetDto>()
            {
                commentGetDto1,
                commentGetDto2,
                commentGetDto3,
            };

            var commentPostRequestModel = new CommentPostRequestModel() { CommentText = "abc", DishId = 3, UserId = 3 };
            var commentPostDto = new CommentPostDto() { UserId = 3, DishId = 3, CommentText = "man" };

            var mockServ = new Mock<ICommentService>();
            mockServ.Setup(x => x.InsertAsync(comment));
            mockServ
                .Setup(x => x.GetCommentsAsync(3))
                .ReturnsAsync(commentsResult);

            var mockAutoMapper = new Mock<IMapper>();
            mockAutoMapper
                .Setup(x => x.Map<CommentPostDto>(It.IsAny<CommentPostRequestModel>()))
                .Returns(commentPostDto);

            var controller = new CommentsController(mockServ.Object, mockAutoMapper.Object);

            //act
            var actionResult = await controller.AddComment(commentPostRequestModel);
            var result = actionResult as OkObjectResult;

            //assert
            Assert.NotNull(actionResult);
            Assert.Equal(200, result.StatusCode);
            Assert.Equal(commentsResult, result.Value);
        }
    }
}
