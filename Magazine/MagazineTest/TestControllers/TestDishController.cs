using Magazine.Application.Dto.Dish;
using Magazine.Application.Dto.Step;
using Magazine.Application.Dto.User;
using Magazine.Application.Services.Interfaces;
using Magazine.Controllers;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using Moq;

using Nancy.Json;

using Newtonsoft.Json;

using FluentAssertions;

namespace MagazineTest.TestControllers;

public class TestDishController
{

    [Fact]
    public async Task GetStepsByDishId_Returns_The_Correct_Number_Of_Steps()
    {
        // Arrange
        var mockService = new Mock<IDishService>();
        mockService.Setup(x => x.GetStepsByDishIdAsync(It.IsAny<GetStepsByDishIdDto>()))
            .ReturnsAsync(GetTestStepsDto());
        var controller = new DishController(mockService.Object);

        // Act
        var actionResult = controller.GetStepsByDishId(1);

        // Assert
        var contentResult = await actionResult as ObjectResult;
        var returnSteps = contentResult?.Value as List<StepDto>;

        returnSteps!.Count.Should().Be(GetTestStepsDto().Count);
        mockService.Verify(mock => mock.GetStepsByDishIdAsync(It.IsAny<GetStepsByDishIdDto>()), Times.Once());
    }

    [Fact]
    public async Task GetStepsByDishId_Returns_The_Correct_Values()
    {
        // Arrange
        var mockService = new Mock<IDishService>();
        mockService.Setup(x => x.GetStepsByDishIdAsync(It.IsAny<GetStepsByDishIdDto>()))
            .ReturnsAsync(GetTestStepsDto());
        var controller = new DishController(mockService.Object);

        // Act
        var actionResult = controller.GetStepsByDishId(1);

        // Assert
        var contentResult = await actionResult as ObjectResult;
        var returnSteps = contentResult?.Value as List<StepDto>;
        var stepDtos = JsonConvert.SerializeObject(GetTestStepsDto());
        var returnStepsJson = JsonConvert.SerializeObject(returnSteps);

        returnStepsJson.Should().Be(stepDtos);
        mockService.Verify(mock => mock.GetStepsByDishIdAsync(It.IsAny<GetStepsByDishIdDto>()), Times.Once());
    }

    [Fact]
    public async Task GetStepsByDishId_Returns_Status_Code_200()
    {
        // Arrange
        var mockService = new Mock<IDishService>();
        mockService
            .Setup(x => x.GetStepsByDishIdAsync(It.IsAny<GetStepsByDishIdDto>()))
            .ReturnsAsync(GetTestStepsDto());
        var controller = new DishController(mockService.Object);

        // Act
        var actionResult = controller.GetStepsByDishId(1);

        // Assert
        var contentResult = await actionResult as OkObjectResult;

        contentResult!.StatusCode.Should().Be(StatusCodes.Status200OK);
        mockService.Verify(mock => mock.GetStepsByDishIdAsync(It.IsAny<GetStepsByDishIdDto>()), Times.Once());
    }

    [Fact]
    public async Task GetStepsByDishId_Returns_Status_Code_204()
    {
        // Arrange
        var mockService = new Mock<IDishService>();
        mockService
            .Setup(x => x.GetStepsByDishIdAsync(It.IsAny<GetStepsByDishIdDto>()))
            .ReturnsAsync(GetTestStepsDto());
        var controller = new DishController(mockService.Object);

        // Act
        var actionResult = controller.GetStepsByDishId(-1);

        // Assert
        var contentResult = await actionResult as StatusCodeResult;

        contentResult!.StatusCode.Should().Be(StatusCodes.Status204NoContent);
        mockService.Verify(mock => mock.GetStepsByDishIdAsync(It.IsAny<GetStepsByDishIdDto>()), Times.Never);
    }

    [Fact]
    public async Task GetAllUsersTags_Returns_The_Correct_Number_Of_Steps()
    {
        // Arrange
        var mockService = new Mock<IDishService>();
        mockService.Setup(x => x.GetTagsUsersAsync(It.IsAny<GetUsersTagsDto>()))
            .ReturnsAsync(GetTestUserTagsDto());
        var controller = new DishController(mockService.Object);

        // Act
        var actionResult = controller.GetAllUsersTags("Vlad");

        // Assert
        var contentResult = await actionResult as ObjectResult;
        var returnSteps = contentResult?.Value as List<UserTagDto>;

        returnSteps!.Count.Should().Be(GetTestStepsDto().Count);
        mockService.Verify(mock => mock.GetTagsUsersAsync(It.IsAny<GetUsersTagsDto>()), Times.Once());
    }

    [Fact]
    public async Task GetAllUsersTags_Returns_Status_Code_200()
    {
        // Arrange
        var mockService = new Mock<IDishService>();
        mockService
            .Setup(x => x.GetTagsUsersAsync(It.IsAny<GetUsersTagsDto>()))
            .ReturnsAsync(GetTestUserTagsDto());
        var controller = new DishController(mockService.Object);

        // Act
        var actionResult = controller.GetAllUsersTags("Vlad");

        // Assert
        var contentResult = await actionResult as OkObjectResult;

        contentResult!.StatusCode.Should().Be(StatusCodes.Status200OK);
        mockService.Verify(mock => mock.GetStepsByDishIdAsync(It.IsAny<GetStepsByDishIdDto>()), Times.Never);
    }


    [Fact]
    public async Task GetDishByIdTestNotNull()
    {
        var mockSer = new Mock<IDishService>();
        mockSer.Setup(x => x.GetIngredientsByIdAsync(2))
            .ReturnsAsync(GetDish());
        var dishController = new DishController(mockSer.Object);

        var result = await dishController.GetDish(2);

        Assert.True(result is OkObjectResult);
    }

    [Fact]
    public async Task GetDishByIdTest200Status()
    {
        var mockSer = new Mock<IDishService>();
        mockSer.Setup(x => x.GetIngredientsByIdAsync(2))
            .ReturnsAsync(GetDish());
        var dishController = new DishController(mockSer.Object);

        var result = await dishController.GetDish(2);

        var contentResult = result as OkObjectResult;
        Assert.Equal(200, contentResult.StatusCode);
    }

    [Fact]
    public async Task GetDishById_Return_Correct_Values()
    {
        var mockSer = new Mock<IDishService>();
        mockSer.Setup(x => x.GetIngredientsByIdAsync(2))
            .ReturnsAsync(GetDish());
        var dishController = new DishController(mockSer.Object);

        var result = await dishController.GetDish(2);

        var contentResult = result as OkObjectResult;
        var js = new JavaScriptSerializer();
        Assert.Equal(js.Serialize(GetDish()), js.Serialize(contentResult.Value));
    }

    [Fact]
    public async Task GetAllSavedDishesByUserId_Return_Correct_Values()
    {
        // Arrange
        var mockService = new Mock<IDishService>();
        mockService
            .Setup(x => x.GetAllSavedDishesByUserIdAsync(It.IsAny<int>()))
            .ReturnsAsync(GetTestSavedDishes());
        var controller = new DishController(mockService.Object);

        // Act
        var actionResult = controller.GetAllSavedDishesByUserId(1);

        // Assert
        var contentResult = await actionResult as OkObjectResult;
        var js = new JavaScriptSerializer();

        contentResult!.StatusCode.Should().Be(StatusCodes.Status200OK);
        js.Serialize(GetTestSavedDishes()).Should().Be(js.Serialize(contentResult.Value));
        mockService.Verify(mock => mock.GetAllSavedDishesByUserIdAsync(It.IsAny<int>()), Times.Once);
    }

    [Fact]
    public async Task GetDishesByUserId_Return_200_Status()
    {
        var mockSer = new Mock<IDishService>();
        mockSer.Setup(x => x.GetAllDishesByUserIdAsync(2, true))
            .ReturnsAsync(GetTestSavedDishes());
        var dishController = new DishController(mockSer.Object);

        var result = await dishController.GetAllDishesByUserIdAsync(2, true);

        var contentResult = result as OkObjectResult;
        Assert.Equal(200, contentResult.StatusCode);
    }

    [Fact]
    public async Task GetDishesByUserId_Return_Correct_Values()
    {
        var mockSer = new Mock<IDishService>();
        mockSer.Setup(x => x.GetAllDishesByUserIdAsync(2, true))
            .ReturnsAsync(GetTestSavedDishes());
        var dishController = new DishController(mockSer.Object);

        var result = await dishController.GetAllDishesByUserIdAsync(2, true);

        var contentResult = result as OkObjectResult;
        var js = new JavaScriptSerializer();
        Assert.Equal(js.Serialize(GetTestSavedDishes()), js.Serialize(contentResult.Value));
    }

    private GetDishByIdDto GetDish()
    {
        var dish = new GetDishByIdDto()
        {
            Id = 2,
            Name = "Egg",
            Image = "egg.png",
            Time = 20,
            Calories = 310,
            Fat = 320,
            Protein = 30,
            Carb = 20,
            DishAuthorId = 3,
            Ingredients = new List<GetIngredientsDto>
            {
                new()
                {
                    ProductName = "Egg",
                    ProductAmount = 50,
                    ProductImage = "egg.png"
                }
            }
        };

        return dish;
    }

    private List<UserTagDto> GetTestUserTagsDto()
    {
        var userList = new List<UserTagDto> {
            new() { Id = 1, Image = "image_1", Name = "Vlad" },
            new() { Id = 2, Image = "image_2", Name = "Vladimir" },
            new() { Id = 3, Image = "image_3", Name = "EgorVladimir" },
            new() { Id = 4, Image = "image_4", Name = "Vladimir Egorov" },
            new() { Id = 5, Image = "image_5", Name = "Nikita Vladimirovich" }
        };

        return userList;
    }

    private List<StepDto> GetTestStepsDto()
    {
        var stepList = new List<StepDto>
        {
            new() { Count = 1, Image = "image_1", Id = 1, Info = "info_1" },
            new() { Count = 2, Image = "image_2", Id = 2, Info = "info_2" },
            new() { Count = 3, Image = "image_3", Id = 3, Info = "info_3" },
            new() { Count = 4, Image = "image_4", Id = 4, Info = "info_4" },
            new() { Count = 5, Image = "image_5", Id = 5, Info = "info_5" }
        };

        return stepList;
    }

    private List<GetDishForUserDto> GetTestSavedDishes()
    {
        var userList = new List<GetDishForUserDto> {
            new() { Id = 1, Image = "image_1", Name = "Pizza" },
            new() { Id = 2, Image = "image_2", Name = "Potato" },
            new() { Id = 3, Image = "image_3", Name = "Soup" },
        };

        return userList;
    }
}
