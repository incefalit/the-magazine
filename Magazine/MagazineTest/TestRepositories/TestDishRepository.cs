﻿using FluentAssertions;
using Magazine.Data.Persistent;
using Magazine.Data.Persistent.Entity;
using Magazine.Data.Persistent.Repository;
using Microsoft.EntityFrameworkCore;

namespace MagazineTest.TestRepositories;

public class TestDishRepository
{
    public readonly DbContextOptions<ApplicationDbContext> Options;

    public TestDishRepository()
    {
        Options = new DbContextOptionsBuilder<ApplicationDbContext>()
            .UseInMemoryDatabase("TestDishDb")
            .Options;
    }

    [Fact]
    public async Task GetAllSavedDishesByUserIdAsync_Return_Steps()
    {
        // Arrange
        var dishContext = new ApplicationDbContext(Options);
        await dishContext.Database.EnsureDeletedAsync();
        var user = new User
        {
            Id = 1,
            Image = "image_1",
            Name = "name_1",
            SavedDishes = new List<Dish>
            {
                new()
                {
                    Id = 1,
                    Publish = true,
                    Name = "name_1",
                    Image = "image_1"
                },
                new()
                {
                    Id = 2,
                    Publish = true,
                    Name = "name_2",
                    Image = "image_2"
                },
                new()
                {
                    Id = 3,
                    Publish = true,
                    Name = "name_3",
                    Image = "image_3"
                }
            }
        };

        dishContext.Users.Add(user);

        await dishContext.SaveChangesAsync();

        var repository = new DishRepository(dishContext);

        // Act
        var actionResult = repository.GetAllSavedDishesByUserIdAsync(1);

        // Assert
        await dishContext.DisposeAsync();
        var contentResult = await actionResult;

        contentResult.Should().NotBeNullOrEmpty();
        contentResult.Count.Should().Be(3);
    }

    [Fact]
    public async Task GetDishByIdRepositoryTest()
    {
        var ingredientContext = new ApplicationDbContext(Options);
        await ingredientContext.Database.EnsureDeletedAsync();
        ingredientContext.Dishes.Add(new Dish
        {
            Id = 2,
            DishAuthorId = 2,
            Image = "egg.png",
            Name = "egg",
            Time = 20,
            Calories = 310,
            Fat = 320,
            Protein = 30,
            Carb = 30,
            DishAuthor = new DishAuthor
            {
                Id = 2,
                UserId = 2,
                User = new User
                {
                    Id = 2,
                    Image = "igor.png",
                    Name = "Igor"
                }
            },

            Ingredients = new List<Ingredient>
            {
                new()
                {
                    Id = 2,
                    DishId = 2,
                    ProductId = "2",
                    ProductAmount = 500,
                    Products = new List<Product>
                    {
                        new()
                        {
                            Id = 2,
                            Image = "imageProduct",
                            Name = "NameProduct",
                            IngredientId = 2
                        }
                    },

                    ProductUnit = new ProductUnit
                    {
                        Id = 2,
                        Unit = "mg"
                    }
                }
            }
        });

        await ingredientContext.SaveChangesAsync();
        var repository = new DishRepository(ingredientContext);

        var actionResult = await repository.GetIngredientAsync(2) as object;

        Assert.NotNull(actionResult);
        Assert.IsAssignableFrom<Dish>(actionResult);
    }

    [Fact]
    public async Task GetDishByUserIdRepositoryTest()
    {
        // Arrange
        var dishContext = new ApplicationDbContext(Options);
        await dishContext.Database.EnsureDeletedAsync();
        var user = new User
        {
            Id = 2,
            Image = "igor.png",
            Name = "Igor"
        };

        var dishAuthor = new DishAuthor
        {
            Id = 1,
            User = user
        };

        dishContext.Dishes.Add(new Dish
        {
            Id = 1,
            Name = "egg",
            Image = "egg.png",
            DishAuthorId = 1,
            Publish = true,
            DishAuthor = dishAuthor
        });

        dishContext.Dishes.Add(new Dish
        {
            Id = 2,
            Name = "milkshake",
            Image = "milkshake.png",
            DishAuthorId = 1,
            Publish = true,
            DishAuthor = dishAuthor
        });

        dishContext.SaveChanges();

        var repository = new DishRepository(dishContext);

        var actionResult = await repository.GetAllDishesByUserIdAsync(2, true);

        Assert.NotNull(actionResult);
        Assert.IsAssignableFrom<List<Dish>>(actionResult);
    }
}