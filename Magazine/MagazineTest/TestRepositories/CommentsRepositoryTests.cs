﻿using Magazine.Data.Persistent;
using Magazine.Data.Persistent.Entity;
using Magazine.Data.Persistent.Repository;

using Microsoft.EntityFrameworkCore;


namespace MagazineTest.TestRepositories
{
    public class CommentsRepositoryTests
    {
        private readonly DbContextOptions<ApplicationDbContext> DbContextOptions;

        public CommentsRepositoryTests()
        {
            DbContextOptions = new DbContextOptionsBuilder<ApplicationDbContext>()
            .UseInMemoryDatabase(databaseName: "TestDb")
            .Options;
        }

        [Fact]
        public async Task GetAllAsync_Return_Comments()
        {
            //arrange 
            var commentContext = new ApplicationDbContext(DbContextOptions);
            commentContext.Database.EnsureDeleted();

            var user = new User { Id = 3, Name = "Tom", Image = "url" };

            var comment1 = new Comment { Id = 1, CommentDate = default, DishId = 3, UserId = 3, CommentText = "abc", User = user };
            var comment2 = new Comment { Id = 2, CommentDate = default, DishId = 3, UserId = 3, CommentText = "abc", User = user };
            var comment3 = new Comment { Id = 3, CommentDate = default, DishId = 3, UserId = 3, CommentText = "abc", User = user };

            commentContext.Comments.Add(comment1);
            commentContext.Comments.Add(comment2);
            commentContext.Comments.Add(comment3);
            commentContext.SaveChanges();

            var repository = new CommentRepository(commentContext);

            //act
            var actionResult = await repository.GetCommentsByDishIdAsync(3);

            //assert
            // var contentResult = actionResult.Result;
            Assert.Equal(3, actionResult.Count);
        }

        [Fact]
        public async Task InsertAsync_Return_Comments()
        {
            //arrange
            var commentContext = new ApplicationDbContext(DbContextOptions);
            commentContext.Database.EnsureDeleted();

            var repository = new CommentRepository(commentContext);

            var user = new User { Id = 3, Name = "Tom", Image = "url" };

            var comment = new Comment { Id = 1, CommentDate = default, DishId = 3, UserId = 3, CommentText = "abc", User = user };

            //act
            repository.InsertAsync(comment);

            //assert
            var comments = commentContext.Comments.Count();
            Assert.Equal(1, comments);
        }
    }
}
