﻿using Magazine.Data.Persistent.Entity;
using Magazine.Data.Persistent.Repository;

using Microsoft.EntityFrameworkCore;

using FluentAssertions;
using Magazine.Data.Persistent;

namespace MagazineTest.TestRepositories;

public class TestUserRepository
{
    public readonly DbContextOptions<ApplicationDbContext> Options;

    public TestUserRepository()
    {
        Options = new DbContextOptionsBuilder<ApplicationDbContext>()
            .UseInMemoryDatabase(databaseName: "TestUserDb")
            .Options;
    }

    [Fact]
    public async Task GetAllUsersTagsAsync_Return_Steps()
    {
        // Arrange
        var userContext = new ApplicationDbContext(Options);

        userContext.Users.Add(new User()
        {
            Id = 1,
            Image = "image_1",
            Name = "name_1"
        });

        userContext.Users.Add(new User()
        {
            Id = 2,
            Image = "image_2",
            Name = "name_2"
        });

        userContext.Users.Add(new User()
        {
            Id = 4,
            Image = "image_3",
            Name = "name_3"
        });

        await userContext.SaveChangesAsync();

        var repository = new UserRepository(userContext);

        // Act
        var actionResult = repository.GetAllUsersTagsAsync("name");

        // Assert
        var contentResult = await actionResult;
        await userContext.DisposeAsync();

        contentResult.Should().NotBeNullOrEmpty();
        contentResult.Count.Should().Be(3);
    }
}