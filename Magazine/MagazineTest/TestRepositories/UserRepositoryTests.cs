﻿using Magazine.Data.Persistent.Entity;
using Magazine.Data.Persistent;
using Magazine.Data.Persistent.Repository;

using Microsoft.EntityFrameworkCore;

using Newtonsoft.Json;

namespace MagazineTest.TestRepositories
{
    public class UserRepositoryTests
    {
        private readonly DbContextOptions<ApplicationDbContext> DbContextOptions;

        public UserRepositoryTests()
        {
            DbContextOptions = new DbContextOptionsBuilder<ApplicationDbContext>()
            .UseInMemoryDatabase(databaseName: "TestDb")
            .Options;
        }

        [Fact]
        public async Task GetUserByIdAsync_Return_User()
        {
            //arrange 
            var userContext = new ApplicationDbContext(DbContextOptions);
            userContext.Database.EnsureDeleted();

            var user = new User
            {
                Id = 6,
                Name = "anton",
                Image = "url",
                Role = new Role(),
                Comments = new List<Comment>(),
                DishAuthors = new List<DishAuthor>(),
                Followers = new List<Follower>(),
                RoleId = 1,
                Friends = new List<Friend>()
            };

            userContext.Users.Add(user);
            userContext.SaveChanges();
            var repository = new UserRepository(userContext);

            //act
            var actionResult = await repository.GetByIdAsync(6);

            //assert
            var object1Json = JsonConvert.SerializeObject(user, Formatting.None, new JsonSerializerSettings()
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            });

            var object2Json = JsonConvert.SerializeObject(actionResult, Formatting.None, new JsonSerializerSettings()
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            });

            Assert.Equal(object1Json, object2Json);
        }

        [Fact]
        public async Task GetByIdWithFollowersAsync_Return_User()
        {
            //arrange 
            var userContext = new ApplicationDbContext(DbContextOptions);
            userContext.Database.EnsureDeleted();

            var user = new User
            {
                Id = 7,
                Name = "anton",
                Image = "url",
                Role = new Role(),
                Comments = new List<Comment>(),
                DishAuthors = new List<DishAuthor>(),
                Followers = new List<Follower>(),
                RoleId = 1,
                Friends = new List<Friend>()
            };

            userContext.Users.Add(user);
            userContext.SaveChanges();
            var repository = new UserRepository(userContext);

            //act
            var actionResult = await repository.GetByIdWithFollowersAsync(7);

            //assert
            var object1Json = JsonConvert.SerializeObject(user, Formatting.None, new JsonSerializerSettings()
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            });

            var object2Json = JsonConvert.SerializeObject(actionResult, Formatting.None, new JsonSerializerSettings()
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            });

            Assert.Equal(object1Json, object2Json);
        }
    }
}
