﻿using Magazine.Data.Persistent;
using Magazine.Data.Persistent.Entity;
using Magazine.Data.Persistent.Repository;

using Microsoft.EntityFrameworkCore;

using FluentAssertions;

namespace MagazineTest.TestRepositories;

public class TestStepRepository
{
    public readonly DbContextOptions<ApplicationDbContext> Options;

    public TestStepRepository()
    {
        Options = new DbContextOptionsBuilder<ApplicationDbContext>()
            .UseInMemoryDatabase(databaseName: "TestStepDb")
            .Options;
    }

    [Fact]
    public async Task GetStepsByDishIdAsync_Return_Steps()
    {
        // Arrange
        var stepContext = new ApplicationDbContext(Options);

        stepContext.Steps.Add(new Step()
        {
            DishId = 1, 
            Count = 1, 
            Id = 1, 
            Image = "image_1",
            Info = "info_1"
        });

        stepContext.Steps.Add(new Step()
        {
            DishId = 1, 
            Count = 2, 
            Id = 2, 
            Image = "image_2", 
            Info = "info_2"
        });

        stepContext.Steps.Add(new Step()
        {
            DishId = 1, 
            Count = 3, 
            Id = 3, 
            Image = "image_3", 
            Info = "info_3"
        });

        await stepContext.SaveChangesAsync();

        var repository = new StepRepository(stepContext);

        // Act
        var actionResult = repository.GetStepsByDishIdAsync(1);

        // Assert
        var contentResult = await actionResult;

        contentResult.Should().NotBeNullOrEmpty();
        contentResult.Count.Should().Be(3);
    }
}