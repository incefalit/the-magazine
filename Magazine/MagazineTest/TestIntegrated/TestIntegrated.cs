﻿using System.Net;

using Microsoft.AspNetCore.Mvc.Testing;

using Magazine.Application.Dto.User;
using MagazineTest.TestWebApplicationFactories;

using Newtonsoft.Json;

using FluentAssertions;

namespace MagazineTest.TestIntegrated
{
    public class DishControllerTests : IClassFixture<TestWebApplicationFactory<Program>>
    {
        private readonly HttpClient _client;

        public DishControllerTests(TestWebApplicationFactory<Program> factory)
        {
            _client = factory.CreateClient(new WebApplicationFactoryClientOptions
            {
                AllowAutoRedirect = false
            });
        }

        [Theory]
        [InlineData("/api/dish/steps/1")]
        public async Task Get_GetStepsByDishIdReturnStatusCode(string url)
        {
            // Act
            var response = await _client.GetAsync(url);

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
        }

        [Theory]
        [InlineData("/api/comments/GetComments?DishId=3")]
        public async Task Get_GetCommentsByDishIdReturnStatusCode_200OK(string url)
		{
            // Act
            var response = await _client.GetAsync(url);

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
        }
		
		[Theory]
        [InlineData("/api/dish/name")]
        public async Task Get_GetAllTagsUsersReturnStatusCode_200OK(string url)
        {
            // Act
            var response = await _client.GetAsync(url);

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
        }

        [Theory]
        [InlineData("/api/dish/name")]
        public async Task Get_GetAllTagsUsersReturnCorrectValues(string url)
        {
            // Act
            var response = await _client.GetAsync(url);

            // Assert
            var responseContent = await response.Content.ReadAsStringAsync();
            var testStepsJson = JsonConvert.SerializeObject(GetTestsUsers()).ToLower();
            responseContent.Should().Be(testStepsJson);
        }

        [Theory]
        [InlineData("/SavedDishes/1")]
        public async Task Get_GetAllSavedDishesByUserIdReturnStatusCode_200OK(string url)
        {
            // Act
            var response = await _client.GetAsync(url);

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
        }

        [Theory]
        [InlineData("/dishes/2?isPublished=true")]
        public async Task Get_GetAllDishesByUserIdReturnStatusCode_200OK(string url)
        {
            // Act
            var response = await _client.GetAsync(url);

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
        }

        [Theory]
        [InlineData("/api/user/GetUser?Id=3")]
        public async Task GetEndpointReturnSuccessStatusCode(string url)
        {
            // Act
            var response = await _client.GetAsync(url);

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
        }

        private static IEnumerable<UserTagDto> GetTestsUsers()
        {
            var users = new List<UserTagDto>
            {
                new()
                {
                    Id = 1,
                    Image = "image_1",
                    Name = "name_1"
                },
                new()
                {
                    Id = 2,
                    Image = "image_2",
                    Name = "name_2"
                },
                new()
                {
                    Id = 3,
                    Image = "image_3",
                    Name = "name_3"
                },
                new()
                {
                    Id = 4,
                    Image = "image_4",
                    Name = "name_3"
                },
                new()
                {
                    Id = 5,
                    Image = "image_5",
                    Name = "name_5"
                }
            };

            return users;
        }
    }
}
