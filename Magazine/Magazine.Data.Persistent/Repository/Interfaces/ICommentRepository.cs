﻿using Magazine.Data.Persistent.Entity;

namespace Magazine.Data.Persistent.Repository.Interfaces
{
    public interface ICommentRepository
    {
        public Task<List<Comment>> GetCommentsByDishIdAsync(int dishId);

        public Task InsertAsync(Comment comment);
        
        public Task SaveAsync();
    }
}
