using Magazine.Data.Persistent.Entity;

namespace Magazine.Data.Persistent.Repository.Interfaces
{
    public interface IUserRepository
    {
        public Task<User> GetByIdAsync(int userId);

		Task<List<User>> GetAllUsersTagsAsync(string name);

        public Task<User> GetByIdWithFollowersAsync(int userId);
    }
}
