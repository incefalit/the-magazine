﻿using Magazine.Data.Persistent.Entity;

namespace Magazine.Data.Persistent.Repository.Interfaces;

public interface IDishRepository
{
    Task<Dish> GetIngredientAsync(int dishId);

    Task<List<Dish>> GetAllDishesByUserIdAsync(int userId, bool isPublished);

    Task<List<Dish>> GetAllSavedDishesByUserIdAsync(int userId);
}