﻿using Magazine.Data.Persistent.Entity;

namespace Magazine.Data.Persistent.Repository.Interfaces;

public interface IStepRepository
{
    Task<List<Step>> GetStepsByDishIdAsync(int dishId);
}