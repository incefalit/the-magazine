﻿using Magazine.Data.Persistent.Entity;
using Magazine.Data.Persistent.Repository.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Magazine.Data.Persistent.Repository;

public class StepRepository : IStepRepository
{
    private readonly ApplicationDbContext _context;

    public StepRepository(ApplicationDbContext context)
    {
        _context = context;
    }

    public async Task<List<Step>> GetStepsByDishIdAsync(int dishId)
    {
        return await _context.Steps
            .AsNoTracking()
            .Where(x => x.DishId == dishId)
            .OrderBy(x => x.Count)
            .ToListAsync();
    }
}