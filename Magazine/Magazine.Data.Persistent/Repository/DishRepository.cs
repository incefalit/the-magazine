﻿using Magazine.Data.Persistent.Entity;
using Magazine.Data.Persistent.Repository.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Magazine.Data.Persistent.Repository;

public class DishRepository : IDishRepository
{
    private readonly ApplicationDbContext _context;

    public DishRepository(ApplicationDbContext context)
    {
        _context = context;
    }

    public async Task<Dish> GetIngredientAsync(int dishId)
    {
        var result = await _context.Dishes
            .AsNoTracking()
            .Include(x => x.Ingredients)
            .ThenInclude(x => x.Products)
            .Include(x => x.Ingredients)
            .ThenInclude(x => x.ProductUnit)
            .Include(x => x.DishAuthor)
            .ThenInclude(x => x.User)
            .FirstOrDefaultAsync(x => x.Id == dishId);

        return result;
    }

    public async Task<List<Dish>> GetAllDishesByUserIdAsync(int userId, bool isPublished)
    {
        var result = await _context.Dishes
            .AsNoTracking()
            .Include(x => x.DishAuthor)
            .ThenInclude(x => x.User)
            .Where(x => x.Publish == isPublished)
            .Where(x => x.DishAuthor.UserId == userId)
            .ToListAsync();

        return result;
    }

    public async Task<List<Dish>> GetAllSavedDishesByUserIdAsync(int userId)
    {
        var result = await _context.Users
            .Include(x => x.SavedDishes)
            .FirstOrDefaultAsync(x => x.Id == userId);

        return result?.SavedDishes;
    }
}