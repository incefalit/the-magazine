﻿using Magazine.Data.Persistent.Entity;
using Magazine.Data.Persistent.Repository.Interfaces;

using Microsoft.EntityFrameworkCore;

namespace Magazine.Data.Persistent.Repository
{
    public class CommentRepository : ICommentRepository
    {
        private readonly ApplicationDbContext _context;

        public CommentRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<List<Comment>> GetCommentsByDishIdAsync(int dishId)
        {
            var result = await _context.Comments.Include(u => u.User)
                .Where(c => c.DishId == dishId)
                .ToListAsync();
            return result;
        }

        public async Task SaveAsync()
        {
            await _context.SaveChangesAsync(true);
        }

        public async Task InsertAsync(Comment comment)
        {
            await _context.AddAsync(comment);
            await _context.SaveChangesAsync(true);
        }
    }
}
