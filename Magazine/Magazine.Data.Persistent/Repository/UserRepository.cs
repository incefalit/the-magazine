using Magazine.Data.Persistent.Entity;
using Magazine.Data.Persistent.Repository.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Magazine.Data.Persistent.Repository;

public class UserRepository : IUserRepository
{
    private readonly ApplicationDbContext _context;

    public UserRepository(ApplicationDbContext context)
    {
        _context = context;
    }

    public async Task<User> GetByIdAsync(int userId)
    {
        return await _context.Users.FirstOrDefaultAsync(u => u.Id == userId);
    }

    public async Task<User> GetByIdWithFollowersAsync(int userId)
    {
        var result = await _context.Users
            .Include(x => x.Followers)
            .Include(x => x.DishAuthors)
            .ThenInclude(x => x.Dishes)
            .FirstOrDefaultAsync(u => u.Id == userId);
        return result;
    }

    public async Task<List<User>> GetAllUsersTagsAsync(string name)
    {
        return await _context.Users
            .AsNoTracking()
            .Where(x => x.Name.Contains(name))
            .OrderBy(x => x.Name)
            .ToListAsync();
    }
}
