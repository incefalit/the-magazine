﻿using Magazine.Data.Persistent.Entity;

using Microsoft.EntityFrameworkCore;

namespace Magazine.Data.Persistent;

public class ApplicationDbContext : DbContext
{
    public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
    {
    }

    public DbSet<Comment> Comments { get; set; }

    public DbSet<Dish> Dishes { get; set; }

    public DbSet<DishAuthor> DishAuthors { get; set; }

    public DbSet<Follower> Followers { get; set; }

    public DbSet<Ingredient> Ingredients { get; set; }

    public DbSet<Product> Products { get; set; }

    public DbSet<ProductUnit> ProductUnits { get; set; }

    public DbSet<Role> Roles { get; set; }

    public DbSet<Step> Steps { get; set; }

    public DbSet<User> Users { get; set; }

    protected override void OnModelCreating(ModelBuilder builder)
    {
        // Comments configuration
        builder.Entity<Comment>()
            .HasKey(x => x.Id);
        builder.Entity<Comment>()
            .HasOne(x => x.Dish)
            .WithMany(n => n.Comments)
            .HasForeignKey(x => x.DishId);
        builder.Entity<Comment>()
            .HasOne(x => x.User)
            .WithMany(n => n.Comments)
            .HasForeignKey(x => x.UserId);

        // Dish configuration
        builder.Entity<Dish>()
            .HasKey(x => x.Id);
        builder.Entity<Dish>()
            .HasOne(x => x.DishAuthor)
            .WithMany(n => n.Dishes)
            .HasForeignKey(x => x.DishAuthorId)
            .OnDelete(DeleteBehavior.NoAction);

        // DishAuthor configuration
        builder.Entity<DishAuthor>()
            .HasKey(x => x.Id);
        builder.Entity<DishAuthor>()
            .HasOne(x => x.User)
            .WithMany(n => n.DishAuthors)
            .HasForeignKey(x => x.UserId);


        // Follower configuration
        builder.Entity<Follower>()
            .HasKey(x => x.Id);

        // Friend configuration
        builder.Entity<Friend>()
            .HasKey(x => x.Id);

        // Ingredient configuration
        builder.Entity<Ingredient>()
            .HasKey(x => x.Id);
        builder.Entity<Ingredient>()
            .HasOne(x => x.Dish)
            .WithMany(n => n.Ingredients)
            .HasForeignKey(x => x.DishId);
        builder.Entity<Ingredient>()
            .HasOne(x => x.ProductUnit)
            .WithMany(n => n.Ingredients)
            .HasForeignKey(x => x.ProductUnitId);

        // Product configuration
        builder.Entity<Product>()
            .HasKey(x => x.Id);
        builder.Entity<Product>()
            .HasOne(x => x.Ingredient)
            .WithMany(n => n.Products)
            .HasForeignKey(x => x.IngredientId);

        // ProductUnit configuration
        builder.Entity<ProductUnit>()
            .HasKey(x => x.Id);

        // Role configuration
        builder.Entity<Role>()
            .HasKey(x => x.Id);

        // Step configuration
        builder.Entity<Step>()
            .HasKey(x => x.Id);
        builder.Entity<Step>()
            .HasOne(x => x.Dish)
            .WithMany(n => n.Steps)
            .HasForeignKey(x => x.DishId);

        // User configuration
        builder.Entity<User>()
            .HasKey(x => x.Id);
        builder.Entity<User>()
            .HasOne(x => x.Role)
            .WithMany(n => n.Users)
            .HasForeignKey(x => x.RoleId);

        // UserFriend configuration
        builder.Entity<User>()
            .HasMany(x => x.Friends)
            .WithMany(n => n.Users)
            .UsingEntity<UserFriend>(
                x => x
                    .HasOne(x => x.Friend)
                    .WithMany()
                    .HasForeignKey(x => x.FriendId),
                x => x
                    .HasOne(x => x.User)
                    .WithMany()
                    .HasForeignKey(x => x.UserId));

        // UserFollower configuration
        builder.Entity<User>()
            .HasMany(x => x.Followers)
            .WithMany(n => n.Users)
            .UsingEntity<UserFollower>(
                x => x
                    .HasOne(x => x.Follower)
                    .WithMany()
                    .HasForeignKey(x => x.FollowerId),
                x => x
                    .HasOne(x => x.User)
                    .WithMany()
                    .HasForeignKey(x => x.UserId));
    }
}