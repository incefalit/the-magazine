﻿
namespace Magazine.Data.Persistent.Entity;

public class Friend
{
    public int Id { get; set; }

    public int UserId { get; set; }

    public virtual List<User> Users { get; set; }
}