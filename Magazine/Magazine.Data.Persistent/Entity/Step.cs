﻿
namespace Magazine.Data.Persistent.Entity;

public class Step
{
    public int Id { get; set; }

    public int DishId { get; set; }

    public int Count { get; set; }

    public string Info { get; set; }

    public string Image { get; set; }

    public virtual Dish Dish { get; set; }
}