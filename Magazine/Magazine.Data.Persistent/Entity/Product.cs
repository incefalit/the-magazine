
namespace Magazine.Data.Persistent.Entity;

public class Product
{
    public int Id { get; set; }

    public string Name { get; set; }

    public string Image { get; set; }

    public int IngredientId { get; set; }

    public virtual Ingredient Ingredient { get; set; }
}