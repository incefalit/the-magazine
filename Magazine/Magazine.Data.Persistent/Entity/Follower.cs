﻿
namespace Magazine.Data.Persistent.Entity;

public class Follower
{
    public int Id { get; set; }

    public int UserId { get; set; }

    public int FollowerId { get; set; }

    public virtual List<User> Users { get; set; }
}