﻿
namespace Magazine.Data.Persistent.Entity;

public class Role
{
    public int Id { get; set; }

    public int UserId { get; set; }

    public int UserRole { get; set; }

    public virtual List<User> Users { get; set; }
}