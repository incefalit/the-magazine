namespace Magazine.Data.Persistent.Entity;

public class ProductUnit
{
    public int Id { get; set; }

    public int ProductId { get; set; }

    public string Unit { get; set; }

    public virtual List<Ingredient> Ingredients { get; set; }

    public virtual List<Product> Products { get; set; }
}