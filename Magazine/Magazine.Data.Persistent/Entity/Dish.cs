namespace Magazine.Data.Persistent.Entity;

public class Dish
{
    public int Id { get; set; }

    public string Name { get; set; }

    public int Time { get; set; }

    public int Calories { get; set; }

    public int Protein { get; set; }

    public int Fat { get; set; }

    public int Carb { get; set; }

    public string Image { get; set; }

    public int DishAuthorId { get; set; }

    public bool Publish { get; set; }

    public DishAuthor DishAuthor { get; set; }

    public virtual List<Comment> Comments { get; set; }

    public virtual List<Ingredient> Ingredients { get; set; }

    public List<Step> Steps { get; set; }

    public List<User> Users { get; set; }
}