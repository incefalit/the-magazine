﻿
namespace Magazine.Data.Persistent.Entity;

public class DishAuthor
{
    public int Id { get; set; }

    public int UserId { get; set; }

    public virtual User User { get; set; }

    public virtual List<Dish> Dishes { get; set; }
}