namespace Magazine.Data.Persistent.Entity;

public class Comment
{
    public int Id { get; set; }

    public int DishId { get; set; }

    public int UserId { get; set; }

    public string CommentText { get; set; }

    public DateTime CommentDate { get; set; }

    public virtual Dish Dish { get; set; }

    public virtual User User { get; set; }
}