﻿namespace Magazine.Data.Persistent.Entity;

public class User
{
    public int Id { get; set; }

    public string Name { get; set; }

    public string Image { get; set; }

    public int RoleId { get; set; }

    public virtual Role Role { get; set; }

    public virtual List<Comment> Comments { get; set; }

    public virtual List<Friend> Friends { get; set; }

    public virtual List<DishAuthor> DishAuthors { get; set; }

    public virtual List<Follower> Followers { get; set; }

    public List<Dish> SavedDishes { get; set; }
}