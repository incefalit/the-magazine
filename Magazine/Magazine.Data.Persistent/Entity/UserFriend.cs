﻿namespace Magazine.Data.Persistent.Entity
{
    public class UserFriend
    {
        public int FriendId { get; set; }

        public int UserId { get; set; }

        public virtual Friend Friend { get; set; }

        public virtual User User { get; set; }
    }
}