namespace Magazine.Data.Persistent.Entity;

public class Ingredient
{
    public int Id { get; set; }

    public int DishId { get; set; }

    public string ProductId { get; set; }

    public int ProductAmount { get; set; }

    public int ProductUnitId { get; set; }

    public virtual Dish Dish { get; set; }

    public virtual ProductUnit ProductUnit { get; set; }

    public virtual List<Product> Products { get; set; }
}