﻿namespace Magazine.Data.Persistent.Entity
{
    public class UserFollower
    {
        public int FollowerId { get; set; }

        public int UserId { get; set; }

        public virtual Follower Follower { get; set; }

        public virtual User User { get; set; }
    }
}