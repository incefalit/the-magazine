using System.Reflection;

using Magazine.Application.Mapper;
using Magazine.Application.Services.Interfaces;
using Magazine.Application.Services.MagazineService;
using Magazine.Data.Persistent;
using Magazine.Data.Persistent.Repository;
using Magazine.Data.Persistent.Repository.Interfaces;
using Magazine.Middleware;
using Magazine.JsonNamingPolicy;

using Microsoft.EntityFrameworkCore;

using Serilog.Events;
using Serilog;
using Magazine.Data.Persistent;

var configuration = new ConfigurationBuilder()
    .SetBasePath(Directory.GetCurrentDirectory())
    .AddJsonFile("appsettings.json")
    .AddJsonFile($"appsettings.{Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") ?? "Production"}.json", true)
    .Build();

Log.Logger = new LoggerConfiguration()
    .ReadFrom.Configuration(configuration)
    .CreateLogger();

var builder = WebApplication.CreateBuilder(args);

builder.WebHost.UseUrls("http://*:8080");
builder.Services.AddControllers().AddJsonOptions(options =>
{
    options.JsonSerializerOptions.PropertyNamingPolicy = new SnakeCaseNamingPolicy();
});

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddCors();

var connectionString = builder.Configuration.GetConnectionString("MagazineDataBase");
var serverVersion = new MySqlServerVersion(serverVersion: ServerVersion.AutoDetect(connectionString));

builder.Services.AddDbContext<ApplicationDbContext>(options =>
    options.UseMySql(connectionString, serverVersion,
        sqlOptions =>
        {
            sqlOptions.MigrationsAssembly(typeof(ApplicationDbContext).GetTypeInfo().Assembly.GetName().Name);
            sqlOptions.EnableRetryOnFailure();

        }));

builder.Services.AddAutoMapper(typeof(AppMappingProfile));
builder.Services.AddTransient<IDishService, DishService>();
builder.Services.AddTransient<IStepRepository, StepRepository>();
builder.Services.AddTransient<ICommentRepository, CommentRepository>();
builder.Services.AddTransient<ICommentService, CommentService>();
builder.Services.AddTransient<IUserService, UserService>();
builder.Services.AddTransient<IDishRepository, DishRepository>();
builder.Services.AddTransient<IUserRepository, UserRepository>();
builder.Host.UseSerilog();

var app = builder.Build();

app.UseMiddleware<ExceptionHandlingMiddleware>();

app.UseHttpsRedirection();

app.UseCors(
    options => options
    .WithOrigins("https://qa-academy-magazine-aaenva.jarvis.syberry.net")
    .AllowAnyHeader()
    );

app.UseSwagger();
app.UseSwaggerUI();
app.UseAuthorization();
app.MapControllers();
app.Run();

public partial class Program { }

SELECT *