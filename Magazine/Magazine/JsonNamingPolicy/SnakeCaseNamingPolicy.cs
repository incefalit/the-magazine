﻿using Magazine.Extensions;

namespace Magazine.JsonNamingPolicy
{
    public class SnakeCaseNamingPolicy : System.Text.Json.JsonNamingPolicy
    {
        public override string ConvertName(string name)
        {
            return name.ToSnakeCase();
        }
    }
}
