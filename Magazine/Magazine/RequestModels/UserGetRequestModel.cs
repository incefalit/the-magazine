﻿using System.ComponentModel.DataAnnotations;

namespace Magazine.RequestModels
{
    public class UserGetRequestModel
    {
        [Required]
        [Range(1,int.MaxValue,ErrorMessage = "User Id should be a positive integer")]
        public int Id { get; set; }
    }
}
