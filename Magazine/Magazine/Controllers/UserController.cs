﻿using Magazine.Application.Services.Interfaces;
using Magazine.RequestModels;

using Microsoft.AspNetCore.Mvc;

using Serilog;

namespace Magazine.Controllers
{
    [Route("api/user")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        [Route("GetUser")]
        [HttpGet]
        public async Task<IActionResult> GetUser([FromQuery] UserGetRequestModel  userGetRequestModel)
        {
            if (!ModelState.IsValid)
            {
                Log.Error("Validaiton is not passed");
                return BadRequest(ModelState);
            }

            var user = await _userService.GetByIdAsync(userGetRequestModel.Id);
            return Ok(user);
        }

        [Route("GetUserProfile")]
        [HttpGet]
        public async Task<IActionResult> GetUserByUserIdWithFollowers([FromQuery] UserGetRequestModel userGetRequestModel)
        {
            if (!ModelState.IsValid)
            {
                Log.Error("Validaiton is not passed");
                return BadRequest(ModelState);
            }

            var user = await _userService.GetByUserByIdWithFollowersAsync(userGetRequestModel.Id);
            return Ok(user);
        }
    }
}
