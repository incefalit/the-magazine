﻿using Magazine.Application.Dto.Dish;
using Magazine.Application.Dto.Step;
using Magazine.Application.Dto.User;
using Magazine.Application.Services.Interfaces;

using Microsoft.AspNetCore.Mvc;

using Serilog;

namespace Magazine.Controllers
{
    [Route("api/dish/")]
    [ApiController]
    public class DishController : ControllerBase
    {
        private readonly IDishService _dishService;

        public DishController(IDishService dishService)
        {
            _dishService = dishService;
        }

        [Route("{dishId:int}")]
        [HttpGet]
        public async Task<IActionResult> GetDish([FromRoute] int dishId)
        {
            if (dishId < 1)
            {
                Log.Error($"Invalid dishId request. Value is no positive int: {0}", dishId);
                return BadRequest();
            }

            var dish = await _dishService.GetIngredientsByIdAsync(dishId);

            return Ok(dish);
        }

        [Route("steps/{dishId:int}")]
        [HttpGet]
        public async Task<IActionResult> GetStepsByDishId([FromRoute] int dishId)
        {
            if (dishId < 1)
            {
                Log.Error($"Bad id: {dishId}");
                return StatusCode(204);
            }
            
            var dto = new GetStepsByDishIdDto 
            {
                DishId = dishId
            };

            var steps = await _dishService.GetStepsByDishIdAsync(dto);

            if (steps.Count != 0)
            {
                return Ok(steps);
            }

            Log.Error($"Steps not found for DishId: {dishId}");
            return NotFound("Steps not found");
        }

        [HttpGet("{name}")]
        public async Task<IActionResult> GetAllUsersTags([FromRoute] string name)
        {
            if (name.Length < 1)
            {
                Log.Error($"Bad name: {name}");
                return StatusCode(204);
            }

            var dto = new GetUsersTagsDto()
            {
                Name = name
            };

            var tags = await _dishService.GetTagsUsersAsync(dto);

            if (tags.Count >= 0)
            {
                return Ok(tags);
            }

            Log.Error($"Users for tag not found: {name}");
            return NotFound("Steps not found");
        }

        [HttpGet("/dishes/{userId:int}")]
        public async Task<IActionResult> GetAllDishesByUserIdAsync([FromRoute] int userId, bool isPublished)
        {
            if (userId < 1)
            {
                Log.Error($"Bad user: {userId}");
                return StatusCode(204);
            }

            List<GetDishForUserDto> dishes;

            if (isPublished)
            {
                dishes = await _dishService.GetAllDishesByUserIdAsync(userId, true);
            }
            else
            {
                dishes = await _dishService.GetAllDishesByUserIdAsync(userId, false);
            }

            if(dishes.Count > 0)
            {
                return Ok(dishes);
            }

            Log.Error($"Dishes not found by Id: {userId}");
            return NotFound("dishes not found");
        }

        [HttpGet("/SavedDishes/{userId:int}")]
        public async Task<IActionResult> GetAllSavedDishesByUserId([FromRoute] int userId)
        {
            if (userId < 1)
            {
                Log.Error($"Bad user: {userId}");
                return StatusCode(204);
            }

            var dishes = await _dishService.GetAllSavedDishesByUserIdAsync(userId);

            if (dishes.Count > 0)
            {
                return Ok(dishes);
            }

            Log.Error($"Dishes not found by Id: {userId}");
            return NotFound("dishes not found");
        }
    }
}
