﻿using AutoMapper;
using Magazine.Application.Dto.Comments;
using Magazine.Application.Services.Interfaces;
using Magazine.RequestModels;

using Microsoft.AspNetCore.Mvc;
using Serilog;

namespace Magazine.Controllers
{
    [Route("api/comments")]
    [ApiController]
    public class CommentsController : ControllerBase
    {
        private readonly ICommentService _commentService;

        private readonly IMapper _mapper;

        public CommentsController(ICommentService commentService, IMapper mapper)
        {
            _commentService = commentService;
            _mapper = mapper;
        }

        [HttpPost, Route("AddComment")]
        public async Task<IActionResult> AddComment(CommentPostRequestModel commentRequestModel)
        {
            if (!ModelState.IsValid)
            {
                Log.Error("Validation is not passed");
                return BadRequest();
            }

            var commentPostDto = _mapper.Map<CommentPostDto>(commentRequestModel);

            await _commentService.InsertAsync(commentPostDto);

            return Ok(await _commentService.GetCommentsAsync(commentPostDto.DishId));
        }


        [HttpGet, Route("GetComments")]
        public async Task<IActionResult> GetCommentsByDishId([FromQuery] CommentGetRequestModel commentRequestModel)
        {
            if (!ModelState.IsValid)
            {
                Log.Error("Validation is not passed");
                return BadRequest();
            }

            var comments = await _commentService.GetCommentsAsync(commentRequestModel.DishId);

            return Ok(comments);
        }
    }
}
