FROM mcr.microsoft.com/dotnet/aspnet:6.0 AS base
WORKDIR /app

FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build
WORKDIR /src
COPY ["./Magazine/Magazine/Magazine.csproj", "Magazine/"]
COPY ["./Magazine/Magazine.Application/Magazine.Application.csproj", "Magazine.Application/"]
COPY ["./Magazine/Magazine.Data.Persistent/Magazine.Data.Persistent.csproj", "Magazine.Data.Persistent/"]
COPY ["./Magazine/MagazineTest/MagazineTest.csproj", "MagazineTest/"]
RUN dotnet restore "Magazine/Magazine.csproj"
COPY . .
WORKDIR "/src/Magazine"
RUN dotnet build "./Magazine/Magazine.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "./Magazine/Magazine.csproj" -c Release -o /app/publish /p:UseAppHost=false

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .

ENTRYPOINT ["dotnet", "Magazine.dll", "--launch-profile Prod"]